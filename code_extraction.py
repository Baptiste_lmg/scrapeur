from selectorlib import Extractor #A installer avec pip - install -> Extraction de données
import requests # Fait le lien avec internet : pour utiliser les url par exemple
import json 
import argparse # Biblio d'analyse de données

# Ce code sert a scraper les données 

# Parser = analyse
argparser = argparse.ArgumentParser()       # Va servir à analyser les données Scraper
argparser.add_argument('url', help='Amazon Product Details URL') # Argument a entrer pour lancer le programme

Extracteur = Extractor.from_yaml_file('Fichier_recuperation_données.yml') # Extrait les données vers ce fichier

user_agent = 'Mozilla/5.0'  ## 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) -> pour ma config (a modif en fonction de la config)

headers = {'User-Agent': user_agent}  # Config du navigateur et de la machine
## {'User-Agent': user_agent} -> A voir pour les autre config (mais normalement pas besoin de modif)

# Télécharger la page choisie pour pouvoir l'analyser
args = argparser.parse_args()
dem = requests.get(args.url, headers=headers)
# Converti le HTML de la page choisie en texte
données = Extracteur.extract(dem.text)

# Print les données
print(json.dumps(données, indent=True))
